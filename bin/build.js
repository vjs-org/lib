// test like so in terminal: find -L . -type f | grep -vE '(bin/|public/|\.git|hello_world)' | grep json | node bin/build.js > public/index.json
const fs = require('fs')
// Work on POSIX and Windows
var stdinBuffer = fs.readFileSync(0); // STDIN_FILENO = 0

var toObject = (str) => {
    var parts = str.split("/")
    var o = {}
    o[parts[0]] = str
    return o
}

var files = stdinBuffer.toString()
                       .split("\n")
                       .filter( (f) => f )
                       .map( (f) => f.substr(2) )

var json = JSON.stringify({graph: files})
console.log(json)
